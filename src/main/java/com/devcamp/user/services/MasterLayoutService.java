package com.devcamp.user.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.MasterLayout;
import com.devcamp.user.repository.MasterLayoutRepository;

@Service
public class MasterLayoutService {
    private final MasterLayoutRepository masterLayoutRepository;

    public MasterLayoutService(MasterLayoutRepository masterLayoutRepository) {
        this.masterLayoutRepository = masterLayoutRepository;
    }

    public List<MasterLayout> getAllMasterLayouts(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<MasterLayout> masterLayoutPage = masterLayoutRepository.findAll(pageable);
            return masterLayoutPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve master layouts", e);
        }
    }

    public MasterLayout getMasterLayoutById(int id) {
        try {
            return masterLayoutRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve master layout by ID: " + id, e);
        }
    }

    public MasterLayout createMasterLayout(MasterLayout masterLayout) {
        try {
            return masterLayoutRepository.save(masterLayout);
        } catch (Exception e) {
            throw new ServiceException("Failed to create master layout", e);
        }
    }

    public MasterLayout updateMasterLayout(MasterLayout masterLayout, int id) {
        try {
            MasterLayout existingMasterLayout = masterLayoutRepository.findById(id).orElse(null);
            if (existingMasterLayout != null) {
                existingMasterLayout.setName(masterLayout.getName());
                existingMasterLayout.setDescription(masterLayout.getDescription());
                existingMasterLayout.setProjectId(masterLayout.getProjectId());
                existingMasterLayout.setAcreage(masterLayout.getAcreage());
                existingMasterLayout.setApartmentList(masterLayout.getApartmentList());
                existingMasterLayout.setPhoto(masterLayout.getPhoto());

                return masterLayoutRepository.save(existingMasterLayout);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update master layout with ID: " + id, e);
        }
    }

    public void deleteMasterLayout(int id) {
        try {
            masterLayoutRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete master layout with ID: " + id, e);
        }
    }
}
