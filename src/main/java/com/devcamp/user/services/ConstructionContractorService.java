package com.devcamp.user.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.ConstructionContractor;
import com.devcamp.user.repository.ConstructionContractorRepository;
import java.util.List;

@Service
public class ConstructionContractorService {
    private final ConstructionContractorRepository constructionContractorRepository;

    public ConstructionContractorService(ConstructionContractorRepository constructionContractorRepository) {
        this.constructionContractorRepository = constructionContractorRepository;
    }

    public List<ConstructionContractor> getConstructionContractors(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<ConstructionContractor> constructionContractorPage = constructionContractorRepository
                    .findAll(pageable);
            return constructionContractorPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve construction contractors", e);
        }
    }

    public ConstructionContractor getConstructionContractorById(int id) {
        try {
            return constructionContractorRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve construction contractor with ID: " + id, e);
        }
    }

    public ConstructionContractor createConstructionContractor(ConstructionContractor constructionContractor) {
        try {
            return constructionContractorRepository.save(constructionContractor);
        } catch (Exception e) {
            throw new ServiceException("Failed to create construction contractor", e);
        }
    }

    public ConstructionContractor updateConstructionContractor(ConstructionContractor constructionContractor, int id) {
        try {
            ConstructionContractor existingConstructionContractor = constructionContractorRepository.findById(id)
                    .orElse(null);

            if (existingConstructionContractor != null) {
                existingConstructionContractor.setName(constructionContractor.getName());
                existingConstructionContractor.setDescription(constructionContractor.getDescription());
                existingConstructionContractor.setProjects(constructionContractor.getProjects());
                existingConstructionContractor.setAddress(constructionContractor.getAddress());
                existingConstructionContractor.setPhone(constructionContractor.getPhone());
                existingConstructionContractor.setSecondPhone(constructionContractor.getSecondPhone());
                existingConstructionContractor.setFax(constructionContractor.getFax());
                existingConstructionContractor.setEmail(constructionContractor.getEmail());
                existingConstructionContractor.setWebsite(constructionContractor.getWebsite());
                existingConstructionContractor.setNote(constructionContractor.getNote());

                return constructionContractorRepository.save(existingConstructionContractor);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update construction contractor with ID: " + id, e);
        }
    }

    public void deleteConstructionContractor(int id) {
        try {
            constructionContractorRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete construction contractor with ID: " + id, e);
        }
    }
}
