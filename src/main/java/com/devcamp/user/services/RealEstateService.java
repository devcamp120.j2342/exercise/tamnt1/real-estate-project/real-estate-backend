package com.devcamp.user.services;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.RealEstate;
import com.devcamp.user.repository.RealEstateRepository;
import com.devcamp.user.response.DatatablePage;
import com.devcamp.user.response.RealEstateCountResponse;
import com.devcamp.user.response.RealEstateResponse;

@Service
public class RealEstateService {
    private final RealEstateRepository realEstateRepository;

    public RealEstateService(RealEstateRepository realEstateRepository) {
        this.realEstateRepository = realEstateRepository;
    }

    public RealEstateResponse getAllRealEstates(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<RealEstate> realEstatePage = realEstateRepository.findAll(pageable);

            RealEstateResponse response = new RealEstateResponse();
            response.setTotalCount(realEstatePage.getTotalElements());
            response.setData(realEstatePage.getContent());

            return response;
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve real estates.", e);
        }
    }

    public DatatablePage<RealEstate> getAllRealestatesDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<RealEstate> realestates = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            realEstateRepository.findAll(pageable).forEach(realestates::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            realEstateRepository.findAll(pageable).forEach(realestates::add);
        }

        long total = realEstateRepository.count();
        DatatablePage<RealEstate> data = new DatatablePage<RealEstate>();
        data.setData(realestates);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    public RealEstateResponse getAllRealEstatesByCustoms(int districtId, int provinceId, int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<RealEstate> realEstatePage = realEstateRepository.getRealEstateByProvinceAndDistrict(districtId,
                    provinceId, pageable);

            RealEstateResponse response = new RealEstateResponse();
            response.setTotalCount(realEstatePage.getTotalElements());
            response.setData(realEstatePage.getContent());

            return response;
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve real estates.", e);
        }
    }

    public List<RealEstateCountResponse> getRealEstateCount() {
        try {
            List<Object[]> realEstateCounts = realEstateRepository.countRealEstateByProvinceAndDistrict();
            List<RealEstateCountResponse> responseList = new ArrayList<>();
            for (Object[] row : realEstateCounts) {
                RealEstateCountResponse response = new RealEstateCountResponse();
                response.set_code((String) row[0]);
                response.set_name((String) row[1]);
                response.setDistrict((String) row[2]);
                response.setDistrictId((Integer) row[3]);
                response.setProvinceId((Integer) row[4]);
                response.setTotalRealEstate((BigInteger) row[5]);
                responseList.add(response);
            }

            return responseList;
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve real estate counts.", e);
        }
    }

    public RealEstateResponse getAllRealEstatesActive(int page, int size) {
        try {

            Pageable pageable = PageRequest.of(page, size);

            Page<RealEstate> realEstatePage = realEstateRepository.getActiveRealEstate(pageable);

            RealEstateResponse response = new RealEstateResponse();
            response.setTotalCount(realEstatePage.getTotalElements());
            response.setData(realEstatePage.getContent());

            return response;
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve active real estates.", e);
        }
    }

    public RealEstateResponse getRealEstateByCustomerId(int id, int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<RealEstate> realEstatePage = realEstateRepository.findByCustomerId(id, pageable);

            RealEstateResponse response = new RealEstateResponse();
            response.setTotalCount(realEstatePage.getTotalElements());
            response.setData(realEstatePage.getContent());

            return response;
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve real estates.", e);
        }
    }

    public RealEstateResponse searchProperties(String title,
            int page, int size) {

        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<RealEstate> realEstatePage = realEstateRepository.searchProperty(title,
                    pageable);

            RealEstateResponse response = new RealEstateResponse();
            response.setTotalCount(realEstatePage.getTotalElements());
            response.setData(realEstatePage.getContent());

            return response;
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve real estates.", e);
        }

    }

    public RealEstateResponse filterProperties(Integer bedroom, Integer bath, Integer provinceId,
            Integer price,
            Pageable pageable) {
        try {
            Page<RealEstate> realEstatePage = realEstateRepository.filterProperty(bedroom, bath, provinceId, price,
                    pageable);

            RealEstateResponse response = new RealEstateResponse();
            response.setTotalCount(realEstatePage.getTotalElements());
            response.setData(realEstatePage.getContent());

            return response;
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve real estates.", e);
        }

    }

    public RealEstate getRealEstateById(int id) {
        try {
            return realEstateRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve real estate with ID: " + id, e);
        }
    }

    public RealEstate setProcess(String request, int id) {
        try {
            RealEstate existingRealEstate = realEstateRepository.findById(id).orElse(null);
            if (existingRealEstate != null) {
                existingRealEstate.setProccess(request);
                realEstateRepository.save(existingRealEstate);
                return existingRealEstate;
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve real estate with ID: " + id, e);
        }
    }

    public RealEstate createRealEstate(RealEstate realEstate) {
        try {
            return realEstateRepository.save(realEstate);
        } catch (Exception e) {
            throw new ServiceException("Failed to create real estate.", e);
        }
    }

    public RealEstate updateRealEstate(RealEstate realEstate, int id) {
        try {
            RealEstate existingRealEstate = realEstateRepository.findById(id).orElse(null);
            if (existingRealEstate != null) {
                existingRealEstate.setTitle(realEstate.getTitle());
                existingRealEstate.setType(realEstate.getType());
                existingRealEstate.setRequest(realEstate.getRequest());
                existingRealEstate.setProvinceId(realEstate.getProvinceId());
                existingRealEstate.setDistrictId(realEstate.getDistrictId());
                existingRealEstate.setWardsId(realEstate.getWardsId());
                existingRealEstate.setStreetId(realEstate.getStreetId());
                existingRealEstate.setProjectId(realEstate.getProjectId());
                existingRealEstate.setAddress(realEstate.getAddress());
                existingRealEstate.setCustomerId(realEstate.getCustomerId());
                existingRealEstate.setPrice(realEstate.getPrice());
                existingRealEstate.setPriceMin(realEstate.getPriceMin());
                existingRealEstate.setPriceTime(realEstate.getPriceTime());
                existingRealEstate.setAcreage(realEstate.getAcreage());
                existingRealEstate.setDirection(realEstate.getDirection());
                existingRealEstate.setTotalFloors(realEstate.getTotalFloors());
                existingRealEstate.setNumberFloors(realEstate.getNumberFloors());
                existingRealEstate.setBath(realEstate.getBath());
                existingRealEstate.setApartCode(realEstate.getApartCode());
                existingRealEstate.setWallArea(realEstate.getWallArea());
                existingRealEstate.setBedroom(realEstate.getBedroom());
                existingRealEstate.setBalcony(realEstate.getBalcony());
                existingRealEstate.setLandscapeView(realEstate.getLandscapeView());
                existingRealEstate.setApartLoca(realEstate.getApartLoca());
                existingRealEstate.setApartType(realEstate.getApartType());
                existingRealEstate.setFurnitureType(realEstate.getFurnitureType());
                existingRealEstate.setPriceRent(realEstate.getPriceRent());
                existingRealEstate.setReturnRate(realEstate.getReturnRate());
                existingRealEstate.setLegalDoc(realEstate.getLegalDoc());
                existingRealEstate.setDescription(realEstate.getDescription());
                existingRealEstate.setWidthY(realEstate.getWidthY());
                existingRealEstate.setLongX(realEstate.getLongX());
                existingRealEstate.setStreetHouse(realEstate.getStreetHouse());
                existingRealEstate.setFsbo(realEstate.getFsbo());
                existingRealEstate.setViewNum(realEstate.getViewNum());
                existingRealEstate.setCreateBy(realEstate.getCreateBy());
                existingRealEstate.setUpdateBy(realEstate.getUpdateBy());
                existingRealEstate.setShape(realEstate.getShape());
                existingRealEstate.setDistanceToFacade(realEstate.getDistanceToFacade());
                existingRealEstate.setAdjacentFacadeNum(realEstate.getAdjacentFacadeNum());
                existingRealEstate.setAdjacentRoad(realEstate.getAdjacentRoad());
                existingRealEstate.setAlleyMinWidth(realEstate.getAlleyMinWidth());
                existingRealEstate.setAdjacentAlleyMinWidth(realEstate.getAdjacentAlleyMinWidth());
                existingRealEstate.setFactor(realEstate.getFactor());
                existingRealEstate.setStructure(realEstate.getStructure());
                existingRealEstate.setDtsxd(realEstate.getDtsxd());
                existingRealEstate.setClcl(realEstate.getClcl());
                existingRealEstate.setCtxdPrice(realEstate.getCtxdPrice());
                existingRealEstate.setCtxdValue(realEstate.getCtxdValue());
                existingRealEstate.setPhoto(realEstate.getPhoto());
                existingRealEstate.setLatitude(realEstate.getLatitude());
                existingRealEstate.setLongitude(realEstate.getLongitude());
                existingRealEstate.setProccess(realEstate.getProccess());
                return realEstateRepository.save(existingRealEstate);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update real estate with ID: " + id, e);
        }
    }

    public void deleteRealEstate(int id) {
        try {
            realEstateRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete real estate with ID: " + id, e);
        }
    }

}
