package com.devcamp.user.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Customer;
import com.devcamp.user.repository.CustomerRepository;
import com.devcamp.user.response.DatatablePage;

@Service
public class CustomerService {
    private final CustomerRepository customerRepository;

    public CustomerService(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;
    }

    public List<Customer> getCustomers(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Customer> pageCustomer = customerRepository.findAll(pageable);
            return pageCustomer.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve customers", e);
        }
    }

    public DatatablePage<Customer> getAllCustomersDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Customer> customers = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            customerRepository.findAll(pageable).forEach(customers::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            customerRepository.findAll(pageable).forEach(customers::add);
        }

        long total = customerRepository.count();
        DatatablePage<Customer> data = new DatatablePage<Customer>();
        data.setData(customers);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    public Customer getCustomerById(int id) {
        try {
            return customerRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve customer with ID: " + id, e);
        }
    }

    public Customer getCustomerByMobile(String mobile) {
        try {
            return customerRepository.findByMobile(mobile);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve customer with ID: " + mobile, e);
        }
    }

    public Customer createCustomer(Customer customer) {
        try {
            return customerRepository.save(customer);
        } catch (Exception e) {
            throw new ServiceException("Failed to create customer", e);
        }
    }

    public Customer updateCustomer(Customer customer, int id) {
        try {
            Customer existingCustomer = customerRepository.findById(id).orElse(null);
            if (existingCustomer != null) {
                existingCustomer.setContactName(customer.getContactName());
                existingCustomer.setContactTitle(customer.getContactTitle());
                existingCustomer.setAddress(customer.getAddress());
                existingCustomer.setMobile(customer.getMobile());
                existingCustomer.setEmail(customer.getEmail());
                existingCustomer.setNote(customer.getNote());

                return customerRepository.save(existingCustomer);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update customer with ID: " + id, e);
        }
    }

    public void deleteCustomer(int id) {
        try {
            customerRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete customer with ID: " + id, e);
        }
    }
}
