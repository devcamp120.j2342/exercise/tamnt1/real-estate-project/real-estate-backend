package com.devcamp.user.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Employee;
import com.devcamp.user.repository.EmployeeRepository;
import java.util.List;

@Service
public class EmployeeService {
    private final EmployeeRepository employeeRepository;

    public EmployeeService(EmployeeRepository employeeRepository) {
        this.employeeRepository = employeeRepository;
    }

    public List<Employee> getAllEmployees(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Employee> pageEmployees = employeeRepository.findAll(pageable);
            return pageEmployees.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve employees", e);
        }
    }

    public Employee getEmployeeById(int employeeId) {
        try {
            return employeeRepository.findById(employeeId).orElse(null);
        } catch (Exception e) {

            throw new ServiceException("Failed to retrieve employee with ID: " + employeeId, e);
        }
    }

    public Employee createEmployee(Employee employee) {
        try {
            return employeeRepository.save(employee);
        } catch (Exception e) {
            throw new ServiceException("Failed to create employee", e);
        }
    }

    public Employee updateEmployee(Employee employee, int employeeId) {
        try {
            Employee existingEmployee = employeeRepository.findById(employeeId).orElse(null);

            if (existingEmployee != null) {
                existingEmployee.setLastName(employee.getLastName());
                existingEmployee.setFirstName(employee.getFirstName());
                existingEmployee.setTitle(employee.getTitle());
                existingEmployee.setTitleOfCourtesy(employee.getTitleOfCourtesy());
                existingEmployee.setBirthDate(employee.getBirthDate());
                existingEmployee.setHireDate(employee.getHireDate());
                existingEmployee.setAddress(employee.getAddress());
                existingEmployee.setCity(employee.getCity());
                existingEmployee.setRegion(employee.getRegion());
                existingEmployee.setPostalCode(employee.getPostalCode());
                existingEmployee.setCountry(employee.getCountry());
                existingEmployee.setHomePhone(employee.getHomePhone());
                existingEmployee.setExtension(employee.getExtension());
                existingEmployee.setPhoto(employee.getPhoto());
                existingEmployee.setNotes(employee.getNotes());
                existingEmployee.setReportsTo(employee.getReportsTo());
                existingEmployee.setUsername(employee.getUsername());
                existingEmployee.setPassword(employee.getPassword());
                existingEmployee.setEmail(employee.getEmail());
                existingEmployee.setActivated(employee.getActivated());
                existingEmployee.setProfile(employee.getProfile());
                existingEmployee.setUserLevel(employee.getUserLevel());

                return employeeRepository.save(existingEmployee);
            }

            return null;
        } catch (Exception e) {

            throw new ServiceException("Failed to update employee with ID: " + employeeId, e);
        }
    }

    public void deleteEmployee(int employeeId) {
        try {
            employeeRepository.deleteById(employeeId);
        } catch (Exception e) {

            throw new ServiceException("Failed to delete employee with ID: " + employeeId, e);
        }
    }
}
