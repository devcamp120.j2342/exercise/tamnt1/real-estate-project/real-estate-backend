package com.devcamp.user.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Street;
import com.devcamp.user.repository.StreetRepository;

@Service
public class StreetService {
    private final StreetRepository streetRepository;

    public StreetService(StreetRepository streetRepository) {
        this.streetRepository = streetRepository;
    }

    public List<Street> getAllStreets(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Street> streetPage = streetRepository.findAll(pageable);
            return streetPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve streets", e);
        }
    }

    public List<Street> getStreetByDistrictId(int districtId, int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            return streetRepository.getStreetByDistrictId(districtId, pageable).getContent();

        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve wards", e);
        }
    }

    public Street getStreetById(int id) {
        try {
            return streetRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve street with ID: " + id, e);
        }
    }

    public Street createStreet(Street street) {
        try {
            return streetRepository.save(street);
        } catch (Exception e) {
            throw new ServiceException("Failed to create street", e);
        }
    }

    public Street updateStreet(Street street, int id) {
        try {
            Street existingStreet = streetRepository.findById(id).orElse(null);
            if (existingStreet != null) {
                existingStreet.setName(street.getName());
                existingStreet.setPrefix(street.getPrefix());
                existingStreet.setProvinceId(street.getProvinceId());
                existingStreet.setDistrictId(street.getDistrictId());
                return streetRepository.save(existingStreet);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update street with ID: " + id, e);
        }
    }

    public void deleteStreet(int id) {
        try {
            streetRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete street with ID: " + id, e);
        }
    }
}
