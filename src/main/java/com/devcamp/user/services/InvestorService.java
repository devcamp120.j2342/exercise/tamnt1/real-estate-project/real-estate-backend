package com.devcamp.user.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Investor;
import com.devcamp.user.repository.InvestorRepository;

@Service
public class InvestorService {
    private final InvestorRepository investorRepository;

    public InvestorService(InvestorRepository investorRepository) {
        this.investorRepository = investorRepository;
    }

    public List<Investor> getAllInvestors(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Investor> investorPage = investorRepository.findAll(pageable);
            return investorPage.getContent();
        } catch (Exception e) {

            throw new ServiceException("Failed to retrieve investors", e);
        }
    }

    public Investor getInvestorById(int id) {
        try {
            return investorRepository.findById(id).orElse(null);
        } catch (Exception e) {

            throw new ServiceException("Failed to retrieve investor with ID: " + id, e);
        }
    }

    public Investor createInvestor(Investor investor) {
        try {
            return investorRepository.save(investor);
        } catch (Exception e) {

            throw new ServiceException("Failed to create investor", e);
        }
    }

    public Investor updateInvestor(Investor investor, int id) {
        try {
            Investor existingInvestor = investorRepository.findById(id).orElse(null);
            if (existingInvestor != null) {
                existingInvestor.setName(investor.getName());
                existingInvestor.setDescription(investor.getDescription());
                existingInvestor.setProjects(investor.getProjects());
                existingInvestor.setAddress(investor.getAddress());
                existingInvestor.setPhone(investor.getPhone());
                existingInvestor.setSecondPhone(investor.getSecondPhone());
                existingInvestor.setFax(investor.getFax());
                existingInvestor.setEmail(investor.getEmail());
                existingInvestor.setWebsite(investor.getWebsite());
                existingInvestor.setNote(investor.getNote());

                return investorRepository.save(existingInvestor);
            }
            return null;
        } catch (Exception e) {

            throw new ServiceException("Failed to update investor with ID: " + id, e);
        }
    }

    public void deleteInvestor(int id) {
        try {
            investorRepository.deleteById(id);
        } catch (Exception e) {

            throw new ServiceException("Failed to delete investor with ID: " + id, e);
        }
    }
}
