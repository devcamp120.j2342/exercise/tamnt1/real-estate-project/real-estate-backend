package com.devcamp.user.services;

import java.math.BigInteger;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.User;
import com.devcamp.user.repository.UserRepository;
import com.devcamp.user.response.UserResponse;

@Service
public class UserService {
    private final UserRepository userRepository;
    @Autowired
    PasswordEncoder encoder;

    public UserService(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    public List<UserResponse> getAllUser(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Object[]> pageUsers = userRepository.findUserWithRole(pageable);
            List<UserResponse> userResponses = new ArrayList<>();
            for (Object[] row : pageUsers.getContent()) {
                BigInteger id = (BigInteger) row[0];
                String username = (String) row[1];
                String email = (String) row[2];
                Integer customerId = (Integer) row[3];
                String roles = (String) row[4];

                UserResponse userResponse = new UserResponse(id, username, email, customerId, roles);
                userResponses.add(userResponse);
            }

            return userResponses;
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve employees", e);
        }
    }

    public User getUserById(Long userId) {
        try {
            return userRepository.findById(userId).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve user with ID: " + userId, e);
        }
    }

    public User createUser(User user) {
        try {
            user.setPassword(encoder.encode(user.getPassword()));
            return userRepository.save(user);
        } catch (Exception e) {
            throw new ServiceException("Failed to create user", e);
        }
    }

    public User updateUser(User user, Long userId) {
        try {
            User existingUser = userRepository.findById(userId).orElse(null);

            if (existingUser != null) {

                existingUser.setUsername(user.getUsername());
                existingUser.setEmail(user.getEmail());
                existingUser.setCustomerId(user.getCustomerId());
                existingUser.setRoles(user.getRoles());

                return userRepository.save(existingUser);
            }

            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update user with ID: " + userId, e);
        }
    }

    public void deleteUser(Long userId) {
        try {
            userRepository.deleteById(userId);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete user with ID: " + userId, e);
        }
    }
}
