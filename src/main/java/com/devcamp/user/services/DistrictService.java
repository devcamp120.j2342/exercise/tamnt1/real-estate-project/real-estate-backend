package com.devcamp.user.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.District;
import com.devcamp.user.repository.DistrictRepository;
import com.devcamp.user.response.DistrictInfo;

import java.util.ArrayList;
import java.util.List;

@Service
public class DistrictService {
    private final DistrictRepository districtRepository;

    public DistrictService(DistrictRepository districtRepository) {
        this.districtRepository = districtRepository;
    }

    public List<District> getAllDistricts(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<District> districtPage = districtRepository.findAll(pageable);
            return districtPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve districts", e);
        }
    }

    public District getDistrictById(int id) {
        try {
            return districtRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve district with ID: " + id, e);
        }
    }

    public List<District> getDistrictByProvinceId(int id, int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            return districtRepository.getDistrictByProvinceId(id, pageable).getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve district with ID: " + id, e);
        }
    }

    public District createDistrict(District district) {
        try {
            return districtRepository.save(district);
        } catch (Exception e) {
            throw new ServiceException("Failed to create district", e);
        }
    }

    public List<DistrictInfo> findProvinceAndDistrict(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Object[]> districtPage = districtRepository.getProvinceAndDistrict(pageable);
            List<DistrictInfo> districtInfoList = new ArrayList<>();

            for (Object[] districtData : districtPage.getContent()) {
                String name = (String) districtData[0];
                String prefix = (String) districtData[1];
                String province = (String) districtData[2];
                Integer id = (Integer) districtData[3];
                DistrictInfo districtInfo = new DistrictInfo(id, name, prefix, province);
                districtInfoList.add(districtInfo);
            }

            return districtInfoList;
        } catch (Exception e) {
            throw new ServiceException("Failed to get districts", e);
        }
    }

    public District updateDistrict(District district, int id) {
        try {
            District existingDistrict = districtRepository.findById(id).orElse(null);

            if (existingDistrict != null) {
                existingDistrict.setName(district.getName());
                existingDistrict.setPrefix(district.getPrefix());
                existingDistrict.setProvinceId(district.getProvinceId());

                return districtRepository.save(existingDistrict);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update district with ID: " + id, e);
        }
    }

    public void deleteDistrict(int id) {
        try {
            districtRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete district with ID: " + id, e);
        }
    }
}
