package com.devcamp.user.services;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.DesignUnit;
import com.devcamp.user.repository.DesignUnitRepository;

import java.util.List;

@Service
public class DesignUnitService {
    private final DesignUnitRepository designUnitRepository;

    public DesignUnitService(DesignUnitRepository designUnitRepository) {
        this.designUnitRepository = designUnitRepository;
    }

    public List<DesignUnit> getDesignUnits(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<DesignUnit> designUnitPage = designUnitRepository.findAll(pageable);
            return designUnitPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve design units", e);
        }
    }

    public DesignUnit getDesignUnitById(int id) {
        try {
            return designUnitRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve design unit with ID: " + id, e);
        }
    }

    public DesignUnit createDesignUnit(DesignUnit designUnit) {
        try {
            return designUnitRepository.save(designUnit);
        } catch (Exception e) {
            throw new ServiceException("Failed to create design unit", e);
        }
    }

    public DesignUnit updateDesignUnit(DesignUnit designUnit, int id) {
        try {
            DesignUnit existingDesignUnit = designUnitRepository.findById(id).orElse(null);

            if (existingDesignUnit != null) {
                existingDesignUnit.setName(designUnit.getName());
                existingDesignUnit.setDescription(designUnit.getDescription());
                existingDesignUnit.setProjects(designUnit.getProjects());
                existingDesignUnit.setAddress(designUnit.getAddress());
                existingDesignUnit.setPhone(designUnit.getPhone());
                existingDesignUnit.setSecondPhone(designUnit.getSecondPhone());
                existingDesignUnit.setFax(designUnit.getFax());
                existingDesignUnit.setEmail(designUnit.getEmail());
                existingDesignUnit.setWebsite(designUnit.getWebsite());
                existingDesignUnit.setNote(designUnit.getNote());

                return designUnitRepository.save(existingDesignUnit);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update design unit with ID: " + id, e);
        }
    }

    public void deleteDesignUnit(int id) {
        try {
            designUnitRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete design unit with ID: " + id, e);
        }
    }
}
