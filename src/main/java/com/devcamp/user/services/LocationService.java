package com.devcamp.user.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Location;
import com.devcamp.user.repository.LocationRepository;

@Service
public class LocationService {
    private final LocationRepository locationRepository;

    public LocationService(LocationRepository locationRepository) {
        this.locationRepository = locationRepository;
    }

    public List<Location> getAllLocations(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Location> locationPage = locationRepository.findAll(pageable);
            return locationPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve locations", e);
        }
    }

    public Location getLocationById(int id) {
        try {
            return locationRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve location with id: " + id, e);
        }
    }

    public Location createLocation(Location location) {
        try {
            return locationRepository.save(location);
        } catch (Exception e) {
            throw new ServiceException("Failed to create location", e);
        }
    }

    public Location updateLocation(Location location, int id) {
        try {
            Location existingLocation = locationRepository.findById(id).orElse(null);
            if (existingLocation != null) {
                existingLocation.setLatitude(location.getLatitude());
                existingLocation.setLongitude(location.getLongitude());
                return locationRepository.save(existingLocation);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update location with id: " + id, e);
        }
    }

    public void deleteLocation(int id) {
        try {
            locationRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete location with id: " + id, e);
        }
    }
}
