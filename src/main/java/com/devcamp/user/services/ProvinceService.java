package com.devcamp.user.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Province;
import com.devcamp.user.repository.ProvinceRepository;

@Service
public class ProvinceService {
    private final ProvinceRepository provinceRepository;

    public ProvinceService(ProvinceRepository provinceRepository) {
        this.provinceRepository = provinceRepository;
    }

    public List<Province> getAllProvinces(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Province> provincePage = provinceRepository.findAll(pageable);
            return provincePage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve provinces", e);
        }
    }

    public Province getProvinceById(int id) {
        try {
            return provinceRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve province with ID: " + id, e);
        }
    }

    public Province createProvince(Province province) {
        try {
            return provinceRepository.save(province);
        } catch (Exception e) {
            throw new ServiceException("Failed to create province", e);
        }
    }

    public Province updateProvince(Province province, int id) {
        try {
            Province existingProvince = provinceRepository.findById(id).orElse(null);
            if (existingProvince != null) {
                existingProvince.setName(province.getName());
                existingProvince.setCode(province.getCode());
                return provinceRepository.save(existingProvince);
            } else {
                return null;
            }
        } catch (Exception e) {
            throw new ServiceException("Failed to update province with ID: " + id, e);
        }
    }

    public void deleteProvince(int id) {
        try {
            provinceRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete province with ID: " + id, e);
        }
    }
}
