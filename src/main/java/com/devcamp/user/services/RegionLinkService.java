package com.devcamp.user.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.RegionLink;
import com.devcamp.user.repository.RegionLinkRepository;

@Service
public class RegionLinkService {
    private final RegionLinkRepository regionLinkRepository;

    public RegionLinkService(RegionLinkRepository regionLinkRepository) {
        this.regionLinkRepository = regionLinkRepository;
    }

    public List<RegionLink> getAllRegionLinks(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<RegionLink> regionLinkPage = regionLinkRepository.findAll(pageable);
            return regionLinkPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve region links", e);
        }
    }

    public RegionLink getRegionLinkById(int id) {
        try {
            return regionLinkRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve region link with ID: " + id, e);
        }
    }

    public RegionLink createRegionLink(RegionLink regionLink) {
        try {
            return regionLinkRepository.save(regionLink);
        } catch (Exception e) {
            throw new ServiceException("Failed to create region link", e);
        }
    }

    public RegionLink updateRegionLink(RegionLink regionLink, int id) {
        try {
            RegionLink existingRegionLink = regionLinkRepository.findById(id).orElse(null);
            if (existingRegionLink != null) {
                existingRegionLink.setName(regionLink.getName());
                existingRegionLink.setDescription(regionLink.getDescription());
                existingRegionLink.setPhoto(regionLink.getPhoto());
                existingRegionLink.setAddress(regionLink.getAddress());
                existingRegionLink.setLatitude(regionLink.getLatitude());
                existingRegionLink.setLongitude(regionLink.getLongitude());
                return regionLinkRepository.save(existingRegionLink);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update region link with ID: " + id, e);
        }
    }

    public void deleteRegionLink(int id) {
        try {
            regionLinkRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete region link with ID: " + id, e);
        }
    }
}
