package com.devcamp.user.services;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Project;
import com.devcamp.user.repository.ProjectRepository;
import com.devcamp.user.response.DatatablePage;

@Service
public class ProjectService {
    private final ProjectRepository projectRepository;

    public ProjectService(ProjectRepository projectRepository) {
        this.projectRepository = projectRepository;
    }

    public List<Project> getAllProjects(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Project> projectPage = projectRepository.findAll(pageable);
            return projectPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve projects", e);
        }
    }

    public DatatablePage<Project> getAllProjectsDataTable(HttpServletRequest request) {
        // get the data
        int size = Integer.parseInt(request.getParameter("length"));
        int page = Integer.parseInt(request.getParameter("start")) / size;
        System.out.println(request.getParameter("order[0][column]"));
        System.out.println("columns[" + request.getParameter("order[0][column]") + "][data]");
        String sortColName = request.getParameter("columns[" + request.getParameter("order[0][column]") + "][data]");
        String order = request.getParameter("order[0][dir]");
        List<Project> projects = new ArrayList<>();
        if (order.equals("desc")) {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).descending());
            projectRepository.findAll(pageable).forEach(projects::add);
        } else {
            Pageable pageable = PageRequest.of(page, size, Sort.by(sortColName).ascending());
            projectRepository.findAll(pageable).forEach(projects::add);
        }

        long total = projectRepository.count();
        DatatablePage<Project> data = new DatatablePage<Project>();
        data.setData(projects);
        data.setRecordsFiltered((int) total);
        data.setRecordsTotal((int) total);
        data.setDraw(Integer.parseInt(request.getParameter("draw")));
        return data;
    }

    public Project getProjectById(int id) {
        try {
            return projectRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve project by ID: " + id, e);
        }
    }

    public Project createProject(Project project) {
        try {
            return projectRepository.save(project);
        } catch (Exception e) {
            throw new ServiceException("Failed to create project", e);
        }
    }

    public Project updateProject(Project project, int id) {
        try {
            Project existingProject = projectRepository.findById(id).orElse(null);

            if (existingProject != null) {
                existingProject.setName(project.getName());
                existingProject.setProvinceId(project.getProvinceId());
                existingProject.setDistrictId(project.getDistrictId());
                existingProject.setWardId(project.getWardId());
                existingProject.setStreetId(project.getStreetId());
                existingProject.setAddress(project.getAddress());
                existingProject.setSlogan(project.getSlogan());
                existingProject.setDescription(project.getDescription());
                existingProject.setAcreage(project.getAcreage());
                existingProject.setConstructArea(project.getConstructArea());
                existingProject.setNumBlock(project.getNumBlock());
                existingProject.setNumFloors(project.getNumFloors());
                existingProject.setNumApartment(project.getNumApartment());
                existingProject.setApartmentArea(project.getApartmentArea());
                existingProject.setInvestor(project.getInvestor());
                existingProject.setConstructionContractor(project.getConstructionContractor());
                existingProject.setDesignUnit(project.getDesignUnit());
                existingProject.setUtilities(project.getUtilities());
                existingProject.setRegionLink(project.getRegionLink());
                existingProject.setPhoto(project.getPhoto());
                existingProject.setLatitude(project.getLatitude());
                existingProject.setLongitude(project.getLongitude());

                return projectRepository.save(existingProject);
            }

            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update project with ID: " + id, e);
        }
    }

    public boolean deleteProject(int id) {
        try {
            projectRepository.deleteById(id);
            return true;
        } catch (Exception e) {
            throw new ServiceException("Failed to delete project with ID: " + id, e);
        }
    }

    public boolean deleteAllProjects() {
        try {
            projectRepository.deleteAll();
            return true;
        } catch (Exception e) {
            throw new ServiceException("Failed to delete all projects", e);
        }
    }
}
