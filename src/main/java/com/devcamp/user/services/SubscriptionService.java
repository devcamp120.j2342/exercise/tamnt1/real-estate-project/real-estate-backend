package com.devcamp.user.services;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Subscription;
import com.devcamp.user.repository.SubscriptionRepository;

@Service
public class SubscriptionService {
    private final SubscriptionRepository subscriptionRepository;

    public SubscriptionService(SubscriptionRepository subscriptionRepository) {
        this.subscriptionRepository = subscriptionRepository;
    }

    public List<Subscription> getAllSubscriptions(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Subscription> subscriptionPage = subscriptionRepository.findAll(pageable);
            return subscriptionPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve subscriptions", e);
        }
    }

    public Subscription getSubscriptionById(int id) {
        try {
            return subscriptionRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve subscription by ID: " + id, e);
        }
    }

    public Subscription createSubscription(Subscription subscription) {
        try {
            return subscriptionRepository.save(subscription);
        } catch (Exception e) {
            throw new ServiceException("Failed to create subscription", e);
        }
    }

    public Subscription updateSubscription(Subscription subscription, int id) {
        try {
            Subscription existingSubscription = subscriptionRepository.findById(id).orElse(null);
            if (existingSubscription != null) {
                existingSubscription.setUser(subscription.getUser());
                existingSubscription.setEndpoint(subscription.getEndpoint());
                existingSubscription.setPublicKey(subscription.getPublicKey());
                existingSubscription.setAuthenticationToken(subscription.getAuthenticationToken());
                existingSubscription.setContentEncoding(subscription.getContentEncoding());

                return subscriptionRepository.save(existingSubscription);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update subscription with ID: " + id, e);
        }
    }

    public void deleteSubscription(int id) {
        try {
            subscriptionRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete subscription with ID: " + id, e);
        }
    }
}
