package com.devcamp.user.services;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Utility;
import com.devcamp.user.repository.UtilityRepository;

@Service
public class UtilityService {
    private final UtilityRepository utilityRepository;

    public UtilityService(UtilityRepository utilityRepository) {
        this.utilityRepository = utilityRepository;
    }

    public List<Utility> getAllUtilities(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Utility> utilityPage = utilityRepository.findAll(pageable);
            return utilityPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve utilities.", e);
        }
    }

    public Utility getUtilityById(int id) {
        try {
            return utilityRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve utility with ID: " + id, e);
        }
    }

    public Utility createUtility(Utility utility) {
        try {
            return utilityRepository.save(utility);
        } catch (Exception e) {
            throw new ServiceException("Failed to create utility.", e);
        }
    }

    public Utility updateUtility(Utility utility, int id) {
        try {
            Utility existingUtility = utilityRepository.findById(id).orElse(null);
            if (existingUtility != null) {
                existingUtility.setName(utility.getName());
                existingUtility.setDescription(utility.getDescription());
                existingUtility.setPhoto(utility.getPhoto());

                return utilityRepository.save(existingUtility);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update utility with ID: " + id, e);
        }
    }

    public void deleteUtility(int id) {
        try {
            utilityRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete utility with ID: " + id, e);
        }
    }
}
