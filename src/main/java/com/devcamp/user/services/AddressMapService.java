package com.devcamp.user.services;

import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.AddressMap;
import com.devcamp.user.repository.AddressMapRepository;

@Service
public class AddressMapService {
    private final AddressMapRepository addressMapRepository;

    public AddressMapService(AddressMapRepository addressMapRepository) {
        this.addressMapRepository = addressMapRepository;
    }

    public List<AddressMap> getAllAddressMaps(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<AddressMap> addressMapPage = addressMapRepository.findAll(pageable);
            return addressMapPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve address maps", e);
        }
    }

    public AddressMap getAddressMapById(int id) {
        try {
            return addressMapRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve address map with ID: " + id, e);
        }
    }

    public AddressMap createAddressMap(AddressMap addressMap) {
        try {
            return addressMapRepository.save(addressMap);
        } catch (Exception e) {
            throw new ServiceException("Failed to create address map", e);
        }
    }

    public AddressMap updateAddressMap(int id, AddressMap addressMap) {
        try {
            AddressMap existingAddressMap = addressMapRepository.findById(id).orElse(null);

            if (existingAddressMap != null) {
                existingAddressMap.setAddress(addressMap.getAddress());
                existingAddressMap.setLat(addressMap.getLat());
                existingAddressMap.setLng(addressMap.getLng());

                return addressMapRepository.save(existingAddressMap);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update address map with ID: " + addressMap.getId(), e);
        }
    }

    public void deleteAddressMap(int id) {
        try {
            addressMapRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete address map with ID: " + id, e);
        }
    }
}
