package com.devcamp.user.services;

import java.util.ArrayList;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Ward;
import com.devcamp.user.repository.WardRepository;
import com.devcamp.user.response.WardInfo;

@Service
public class WardService {
    private final WardRepository wardRepository;

    public WardService(WardRepository wardRepository) {
        this.wardRepository = wardRepository;
    }

    public List<Ward> getAllWards(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Ward> wardPage = wardRepository.findAll(pageable);
            return wardPage.getContent();
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve wards", e);
        }
    }

    public List<Ward> getWardsByDistrictId(int districtId, int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            return wardRepository.getWardByDistrictId(districtId, pageable).getContent();

        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve wards", e);
        }
    }

    public List<WardInfo> findWardWithDistrictAndProvince(int page, int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            Page<Object[]> wardPage = wardRepository.getWardWithDistrictAndProvince(pageable);
            List<WardInfo> wardInfoList = new ArrayList<>();

            for (Object[] districtData : wardPage.getContent()) {
                String name = (String) districtData[0];
                String prefix = (String) districtData[1];
                String district = (String) districtData[2];
                String province = (String) districtData[3];
                Integer id = (Integer) districtData[4];
                WardInfo wardInfo = new WardInfo(id, name, prefix, district, province);
                wardInfoList.add(wardInfo);
            }
            return wardInfoList;
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve wards", e);
        }
    }

    public Ward getWardById(int id) {
        try {
            return wardRepository.findById(id).orElse(null);
        } catch (Exception e) {
            throw new ServiceException("Failed to retrieve ward with ID: " + id, e);
        }
    }

    public Ward createWard(Ward ward) {
        try {
            return wardRepository.save(ward);
        } catch (Exception e) {
            throw new ServiceException("Failed to create ward", e);
        }
    }

    public Ward updateWard(Ward ward, int id) {
        try {
            Ward existingWard = wardRepository.findById(id).orElse(null);
            if (existingWard != null) {
                existingWard.setName(ward.getName());
                existingWard.setPrefix(ward.getPrefix());
                existingWard.setProvinceId(ward.getProvinceId());
                existingWard.setDistrictId(ward.getDistrictId());

                return wardRepository.save(existingWard);
            }
            return null;
        } catch (Exception e) {
            throw new ServiceException("Failed to update ward with ID: " + id, e);
        }
    }

    public void deleteWard(int id) {
        try {
            wardRepository.deleteById(id);
        } catch (Exception e) {
            throw new ServiceException("Failed to delete ward with ID: " + id, e);
        }
    }
}
