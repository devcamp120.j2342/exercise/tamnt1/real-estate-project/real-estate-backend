package com.devcamp.user.response;

import java.math.BigInteger;
import java.util.List;

public class UserResponse {
    private BigInteger id;
    private String username;
    private String email;
    private Integer customerId;
    private String roles;

    public UserResponse() {
    }

    public UserResponse(BigInteger id, String username, String email, Integer customerId, String roles) {
        this.id = id;
        this.username = username;
        this.email = email;
        this.customerId = customerId;
        this.roles = roles;
    }

    public BigInteger getId() {
        return id;
    }

    public void setId(BigInteger id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

}