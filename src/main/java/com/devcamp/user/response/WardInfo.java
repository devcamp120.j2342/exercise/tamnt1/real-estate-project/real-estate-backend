package com.devcamp.user.response;

public class WardInfo {
    private Integer id;
    private String name;
    private String prefix;
    private String district;
    private String province;

    public WardInfo() {
    }

    public WardInfo(String name, String prefix, String district, String province) {
        this.name = name;
        this.prefix = prefix;
        this.district = district;
        this.province = province;
    }

    public WardInfo(Integer id, String name, String prefix, String district, String province) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.district = district;
        this.province = province;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
