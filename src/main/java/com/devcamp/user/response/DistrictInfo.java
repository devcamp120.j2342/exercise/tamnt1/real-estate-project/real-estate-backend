package com.devcamp.user.response;

public class DistrictInfo {
    private Integer id;
    private String name;
    private String prefix;
    private String province;

    public DistrictInfo() {
    }

    public DistrictInfo(Integer id, String name, String prefix, String province) {
        this.id = id;
        this.name = name;
        this.prefix = prefix;
        this.province = province;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPrefix() {
        return prefix;
    }

    public void setPrefix(String prefix) {
        this.prefix = prefix;
    }

    public String getProvince() {
        return province;
    }

    public void setProvince(String province) {
        this.province = province;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

}
