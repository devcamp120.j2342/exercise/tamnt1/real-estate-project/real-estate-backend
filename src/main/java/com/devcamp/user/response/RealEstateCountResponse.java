package com.devcamp.user.response;

import java.math.BigInteger;

public class RealEstateCountResponse {
    private String _code;
    private String _name;
    private String district;
    private Integer districtId;
    private Integer provinceId;
    private BigInteger totalRealEstate;

    public RealEstateCountResponse(String _code, String _name, String district, Integer districtId,
            Integer provinceId, BigInteger totalRealEstate) {
        this._code = _code;
        this._name = _name;
        this.district = district;
        this.districtId = districtId;
        this.provinceId = provinceId;
        this.totalRealEstate = totalRealEstate;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public RealEstateCountResponse() {
    }

    public String get_code() {
        return _code;
    }

    public void set_code(String _code) {
        this._code = _code;
    }

    public String get_name() {
        return _name;
    }

    public void set_name(String _name) {
        this._name = _name;
    }

    public String getDistrict() {
        return district;
    }

    public void setDistrict(String district) {
        this.district = district;
    }

    public BigInteger getTotalRealEstate() {
        return totalRealEstate;
    }

    public void setTotalRealEstate(BigInteger totalRealEstate) {
        this.totalRealEstate = totalRealEstate;
    }

}
