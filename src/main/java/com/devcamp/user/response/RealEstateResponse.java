package com.devcamp.user.response;

import java.util.List;

import com.devcamp.user.models.RealEstate;

public class RealEstateResponse {
    private long totalCount;
    private List<RealEstate> data;

    public RealEstateResponse() {
    }

    public RealEstateResponse(long totalCount, List<RealEstate> data) {
        this.totalCount = totalCount;
        this.data = data;
    }

    public long getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(long totalCount) {
        this.totalCount = totalCount;
    }

    public List<RealEstate> getData() {
        return data;
    }

    public void setData(List<RealEstate> data) {
        this.data = data;
    }

}