package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.user.models.Subscription;

public interface SubscriptionRepository extends JpaRepository<Subscription, Integer> {

}
