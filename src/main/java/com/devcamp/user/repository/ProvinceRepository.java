package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.user.models.Province;

public interface ProvinceRepository extends JpaRepository<Province, Integer> {

}
