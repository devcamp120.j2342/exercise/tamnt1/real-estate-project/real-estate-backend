package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.user.models.MasterLayout;

public interface MasterLayoutRepository extends JpaRepository<MasterLayout, Integer> {

}
