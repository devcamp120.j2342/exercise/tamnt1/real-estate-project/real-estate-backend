package com.devcamp.user.repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.devcamp.user.models.RealEstate;

public interface RealEstateRepository extends JpaRepository<RealEstate, Integer> {
        Page<RealEstate> findByCustomerId(@Param("id") Integer id,
                        Pageable pageable);

        @Query("SELECT re FROM RealEstate re WHERE re.proccess = 'active'")
        Page<RealEstate> getActiveRealEstate(Pageable pageable);

        @Query(value = "SELECT * FROM real_estate.realestate r " +
                        "WHERE r.province_id = :provinceId AND r.district_id = :districtId", countQuery = "SELECT COUNT(*) FROM real_estate.realestate r "
                                        +
                                        "WHERE r.province_id = :provinceId AND r.district_id = :districtId", nativeQuery = true)
        Page<RealEstate> getRealEstateByProvinceAndDistrict(@Param("districtId") Integer districtId,
                        @Param("provinceId") Integer provinceId,
                        Pageable pageable);

        @Query(value = "SELECT * FROM realestate re " +
                        "WHERE LOWER(re.title) LIKE CONCAT('%', LOWER(:title), '%') ", nativeQuery = true)
        Page<RealEstate> searchProperty(@Param("title") String title,
                        Pageable pageable);

        @Query(value = "SELECT * FROM realestate re " +
                        "WHERE (:bedroom IS NULL OR re.bedroom = :bedroom) " +
                        "AND (:bath IS NULL OR re.bath = :bath) " +
                        "AND (:provinceId IS NULL OR re.province_id = :provinceId) " +
                        "AND (:price IS NULL OR re.price > :price)", nativeQuery = true)
        Page<RealEstate> filterProperty(@Param("bedroom") Integer bedroom, @Param("bath") Integer bath,
                        @Param("provinceId") Integer provinceId, @Param("price") Integer price,
                        Pageable pageable);

        @Query(value = "SELECT p._code, p._name, d._name AS district,   d.id as districtId, p.id as provinceId, COUNT(r.id) AS totalRealEstate "
                        +
                        "FROM real_estate.realestate r " +
                        "INNER JOIN real_estate.province p ON r.province_id = p.id " +
                        "INNER JOIN real_estate.district d ON r.district_id = d.id " +
                        "GROUP BY p._name, p._code, d._name,d.id,p.id", nativeQuery = true)
        List<Object[]> countRealEstateByProvinceAndDistrict();

}
