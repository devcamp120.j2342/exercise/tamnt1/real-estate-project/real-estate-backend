package com.devcamp.user.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.user.models.Employee;

public interface EmployeeRepository extends JpaRepository<Employee, Integer> {
    Optional<Employee> findByUsername(String username);

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);
}
