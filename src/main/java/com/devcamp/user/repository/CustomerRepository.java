package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.user.models.Customer;

public interface CustomerRepository extends JpaRepository<Customer, Integer> {

    Customer findByMobile(String mobile);
}