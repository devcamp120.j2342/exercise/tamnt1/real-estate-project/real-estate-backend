package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.user.models.AddressMap;

public interface AddressMapRepository extends JpaRepository<AddressMap, Integer> {

}
