package com.devcamp.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.user.models.Street;

public interface StreetRepository extends JpaRepository<Street, Integer> {
    @Query(value = "SELECT t FROM Street t WHERE t.districtId = ?1")
    Page<Street> getStreetByDistrictId(int districtId, Pageable pageable);
}