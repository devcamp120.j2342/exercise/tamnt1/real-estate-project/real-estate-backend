package com.devcamp.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.user.models.District;

public interface DistrictRepository extends JpaRepository<District, Integer> {

    @Query(value = "SELECT d._name,d._prefix , p._name as provinceName, d.id FROM district d INNER JOIN province p ON d._province_id = p.id", nativeQuery = true)
    Page<Object[]> getProvinceAndDistrict(Pageable pageable);

    @Query(value = "SELECT d FROM District d WHERE d.provinceId = ?1")
    Page<District> getDistrictByProvinceId(int provinceId, Pageable pageable);
}
