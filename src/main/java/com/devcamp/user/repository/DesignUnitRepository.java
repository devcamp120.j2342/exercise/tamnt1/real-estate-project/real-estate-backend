package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.user.models.DesignUnit;

public interface DesignUnitRepository extends JpaRepository<DesignUnit, Integer> {

}
