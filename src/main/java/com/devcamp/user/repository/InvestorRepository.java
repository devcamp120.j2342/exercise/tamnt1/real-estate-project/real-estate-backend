package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.user.models.Investor;

public interface InvestorRepository extends JpaRepository<Investor, Integer> {

}
