package com.devcamp.user.repository;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.devcamp.user.models.Ward;

public interface WardRepository extends JpaRepository<Ward, Integer> {

    @Query(value = "SELECT w._name, w._prefix, CONCAT(d._prefix, ' ', d._name) as district, p._name as province, w.id "
            +
            "FROM ward w " +
            "INNER JOIN district d ON w._district_id = d.id " +
            "INNER JOIN province p ON w._province_id = p.id", nativeQuery = true)

    Page<Object[]> getWardWithDistrictAndProvince(Pageable pageable);

    @Query(value = "SELECT w FROM Ward w WHERE w.districtId = ?1")
    Page<Ward> getWardByDistrictId(int districtId, Pageable pageable);
}
