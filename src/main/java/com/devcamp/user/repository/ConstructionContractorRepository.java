package com.devcamp.user.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.devcamp.user.models.ConstructionContractor;

public interface ConstructionContractorRepository extends JpaRepository<ConstructionContractor, Integer> {

}