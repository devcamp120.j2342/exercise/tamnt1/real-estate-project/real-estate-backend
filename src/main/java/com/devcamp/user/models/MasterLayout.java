package com.devcamp.user.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.sql.Timestamp;
import java.util.Date;

@Entity
@Table(name = "master_layout")
@EntityListeners(AuditingEntityListener.class)
public class MasterLayout {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(nullable = false, length = 1000)
    private String name;

    @Column(length = 5000)
    private String description;

    @Column(name = "project_id", nullable = false)
    private int projectId;

    @Column(precision = 12, scale = 2)
    private Double acreage;

    @Column(name = "apartment_list", length = 1000)
    private String apartmentList;

    @Column(length = 5000)
    private String photo;

    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    @Column(name = "date_create", nullable = false, updatable = false)
    private Date dateCreate;
    @Temporal(TemporalType.TIMESTAMP)
    @LastModifiedDate
    @Column(name = "date_update")
    private Date dateUpdate;

    public MasterLayout() {
    }

    public MasterLayout(String name, String description, int projectId, Double acreage, String apartmentList,
            String photo) {
        this.name = name;
        this.description = description;
        this.projectId = projectId;
        this.acreage = acreage;
        this.apartmentList = apartmentList;
        this.photo = photo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getProjectId() {
        return projectId;
    }

    public void setProjectId(int projectId) {
        this.projectId = projectId;
    }

    public Double getAcreage() {
        return acreage;
    }

    public void setAcreage(Double acreage) {
        this.acreage = acreage;
    }

    public String getApartmentList() {
        return apartmentList;
    }

    public void setApartmentList(String apartmentList) {
        this.apartmentList = apartmentList;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public Date getDateUpdate() {
        return dateUpdate;
    }

    public void setDateUpdate(Timestamp dateUpdate) {
        this.dateUpdate = dateUpdate;
    }

}
