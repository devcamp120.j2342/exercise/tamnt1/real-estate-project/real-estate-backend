package com.devcamp.user.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDate;

@Entity
@Table(name = "employees")
public class Employee {

	@Id
	@Column(name = "EmployeeID")
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int employeeId;

	@Column(name = "LastName", length = 20)
	private String lastName;

	@Column(name = "FirstName", length = 10)
	private String firstName;

	@Column(name = "Title", length = 30)
	private String title;

	@Column(name = "TitleOfCourtesy", length = 25)
	private String titleOfCourtesy;

	@Column(name = "BirthDate")
	private LocalDate birthDate;

	@Column(name = "HireDate")
	private LocalDate hireDate;

	@Column(name = "Address", length = 60)
	private String address;

	@Column(name = "City", length = 15)
	private String city;

	@Column(name = "Region", length = 15)
	private String region;

	@Column(name = "PostalCode", length = 10)
	private String postalCode;

	@Column(name = "Country", length = 15)
	private String country;

	@Column(name = "HomePhone", length = 24)
	private String homePhone;

	@Column(name = "Extension", length = 4)
	private String extension;

	@Column(name = "Photo", length = 200)
	private String photo;

	@Column(name = "Notes", columnDefinition = "LONGTEXT")
	private String notes;

	@Column(name = "ReportsTo")
	private Integer reportsTo;

	@Column(name = "Username", length = 50)
	private String username;

	@Column(name = "Password", length = 255)
	private String password;

	@Column(name = "Email", length = 255)
	private String email;

	@Column(name = "Activated")
	private String activated;

	@Column(name = "Profile", columnDefinition = "LONGTEXT")
	private String profile;

	@Column(name = "UserLevel")
	private String userLevel;

	public Employee() {
	}

	public Employee(String lastName, String firstName, String title, String titleOfCourtesy, LocalDate birthDate,
			LocalDate hireDate, String address, String city, String region, String postalCode, String country,
			String homePhone, String extension, String photo, String notes, Integer reportsTo, String username,
			String password, String email, String activated, String profile, String userLevel) {
		this.lastName = lastName;
		this.firstName = firstName;
		this.title = title;
		this.titleOfCourtesy = titleOfCourtesy;
		this.birthDate = birthDate;
		this.hireDate = hireDate;
		this.address = address;
		this.city = city;
		this.region = region;
		this.postalCode = postalCode;
		this.country = country;
		this.homePhone = homePhone;
		this.extension = extension;
		this.photo = photo;
		this.notes = notes;
		this.reportsTo = reportsTo;
		this.username = username;
		this.password = password;
		this.email = email;
		this.activated = activated;
		this.profile = profile;
		this.userLevel = userLevel;
	}

	public int getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(int employeeId) {
		this.employeeId = employeeId;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getTitleOfCourtesy() {
		return titleOfCourtesy;
	}

	public void setTitleOfCourtesy(String titleOfCourtesy) {
		this.titleOfCourtesy = titleOfCourtesy;
	}

	public LocalDate getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(LocalDate birthDate) {
		this.birthDate = birthDate;
	}

	public LocalDate getHireDate() {
		return hireDate;
	}

	public void setHireDate(LocalDate hireDate) {
		this.hireDate = hireDate;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getRegion() {
		return region;
	}

	public void setRegion(String region) {
		this.region = region;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCountry() {
		return country;
	}

	public void setCountry(String country) {
		this.country = country;
	}

	public String getHomePhone() {
		return homePhone;
	}

	public void setHomePhone(String homePhone) {
		this.homePhone = homePhone;
	}

	public String getExtension() {
		return extension;
	}

	public void setExtension(String extension) {
		this.extension = extension;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Integer getReportsTo() {
		return reportsTo;
	}

	public void setReportsTo(Integer reportsTo) {
		this.reportsTo = reportsTo;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getActivated() {
		return activated;
	}

	public void setActivated(String activated) {
		this.activated = activated;
	}

	public String getProfile() {
		return profile;
	}

	public void setProfile(String profile) {
		this.profile = profile;
	}

	public String getUserLevel() {
		return userLevel;
	}

	public void setUserLevel(String userLevel) {
		this.userLevel = userLevel;
	}

}
