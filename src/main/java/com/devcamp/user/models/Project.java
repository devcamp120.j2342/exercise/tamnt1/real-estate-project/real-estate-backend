package com.devcamp.user.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "project")
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private int id;

    @Column(name = "_name", length = 200)
    private String name;

    @Column(name = "_province_id")
    private Integer provinceId;

    @Column(name = "_district_id")
    private Integer districtId;

    @Column(name = "_ward_id")
    private Integer wardId;

    @Column(name = "_street_id")
    private Integer streetId;

    @Column(name = "address", length = 1000)
    private String address;

    @Column(name = "slogan", columnDefinition = "MEDIUMTEXT")
    private String slogan;

    @Column(name = "description", columnDefinition = "TEXT")
    private String description;

    @Column(name = "acreage", precision = 20, scale = 2)
    private Double acreage;

    @Column(name = "construct_area", precision = 20, scale = 2)
    private Double constructArea;

    @Column(name = "num_block")
    private Short numBlock;

    @Column(name = "num_floors", length = 500)
    private String numFloors;

    @Column(name = "num_apartment", nullable = false)
    private Integer numApartment;

    @Column(name = "apartment_area", length = 500)
    private String apartmentArea;

    @Column(name = "investor", nullable = false)
    private Integer investor;

    @Column(name = "construction_contractor")
    private Integer constructionContractor;

    @Column(name = "design_unit")
    private Integer designUnit;

    @Column(name = "utilities", length = 1000)
    private String utilities;

    @Column(name = "region_link", length = 1000)
    private String regionLink;

    @Column(name = "photo", length = 5000)
    private String photo;

    @Column(name = "_lat", precision = 10, scale = 6)
    private Double latitude;

    @Column(name = "_lng", precision = 10, scale = 6)
    private Double longitude;

    public Project() {
    }

    public Project(String name, Integer provinceId, Integer districtId, Integer wardId, Integer streetId,
            String address, String slogan, String description, Double acreage, Double constructArea, Short numBlock,
            String numFloors, Integer numApartment, String apartmentArea, Integer investor,
            Integer constructionContractor, Integer designUnit, String utilities, String regionLink, String photo,
            Double latitude, Double longitude) {

        this.name = name;
        this.provinceId = provinceId;
        this.districtId = districtId;
        this.wardId = wardId;
        this.streetId = streetId;
        this.address = address;
        this.slogan = slogan;
        this.description = description;
        this.acreage = acreage;
        this.constructArea = constructArea;
        this.numBlock = numBlock;
        this.numFloors = numFloors;
        this.numApartment = numApartment;
        this.apartmentArea = apartmentArea;
        this.investor = investor;
        this.constructionContractor = constructionContractor;
        this.designUnit = designUnit;
        this.utilities = utilities;
        this.regionLink = regionLink;
        this.photo = photo;
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getWardId() {
        return wardId;
    }

    public void setWardId(Integer wardId) {
        this.wardId = wardId;
    }

    public Integer getStreetId() {
        return streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getSlogan() {
        return slogan;
    }

    public void setSlogan(String slogan) {
        this.slogan = slogan;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getAcreage() {
        return acreage;
    }

    public void setAcreage(Double acreage) {
        this.acreage = acreage;
    }

    public Double getConstructArea() {
        return constructArea;
    }

    public void setConstructArea(Double constructArea) {
        this.constructArea = constructArea;
    }

    public Short getNumBlock() {
        return numBlock;
    }

    public void setNumBlock(Short numBlock) {
        this.numBlock = numBlock;
    }

    public String getNumFloors() {
        return numFloors;
    }

    public void setNumFloors(String numFloors) {
        this.numFloors = numFloors;
    }

    public Integer getNumApartment() {
        return numApartment;
    }

    public void setNumApartment(Integer numApartment) {
        this.numApartment = numApartment;
    }

    public String getApartmentArea() {
        return apartmentArea;
    }

    public void setApartmentArea(String apartmentArea) {
        this.apartmentArea = apartmentArea;
    }

    public Integer getInvestor() {
        return investor;
    }

    public void setInvestor(Integer investor) {
        this.investor = investor;
    }

    public Integer getConstructionContractor() {
        return constructionContractor;
    }

    public void setConstructionContractor(Integer constructionContractor) {
        this.constructionContractor = constructionContractor;
    }

    public Integer getDesignUnit() {
        return designUnit;
    }

    public void setDesignUnit(Integer designUnit) {
        this.designUnit = designUnit;
    }

    public String getUtilities() {
        return utilities;
    }

    public void setUtilities(String utilities) {
        this.utilities = utilities;
    }

    public String getRegionLink() {
        return regionLink;
    }

    public void setRegionLink(String regionLink) {
        this.regionLink = regionLink;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
