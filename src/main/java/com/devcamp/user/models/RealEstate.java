package com.devcamp.user.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.math.BigDecimal;
import java.util.Date;

@Entity
@Table(name = "realestate")
@EntityListeners(AuditingEntityListener.class)
public class RealEstate {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "title", length = 2000)
    private String title;

    @Column(name = "type")
    private Integer type;

    @Column(name = "request")
    private Integer request;

    @Column(name = "province_id")
    private Integer provinceId;

    @Column(name = "district_id")
    private Integer districtId;

    @Column(name = "wards_id")
    private Integer wardsId;

    @Column(name = "street_id")
    private Integer streetId;

    @Column(name = "project_id")
    private Integer projectId;

    @Column(name = "address", nullable = false, length = 2000)
    private String address;

    @Column(name = "customer_id")
    private Integer customerId;

    @Column(name = "price")
    private BigDecimal price;

    @Column(name = "price_min")
    private BigDecimal priceMin;

    @Column(name = "price_time")
    private Integer priceTime;

    @Column(name = "date_create", nullable = false)
    @Temporal(TemporalType.TIMESTAMP)
    @CreatedDate
    private Date dateCreate;

    @Column(name = "acreage")
    private BigDecimal acreage;

    @Column(name = "direction")
    private Integer direction;

    @Column(name = "total_floors")
    private Integer totalFloors;

    @Column(name = "number_floors")
    private Integer numberFloors;

    @Column(name = "bath")
    private Integer bath;

    @Column(name = "apart_code", length = 10)
    private String apartCode;

    @Column(name = "wall_area")
    private BigDecimal wallArea;

    @Column(name = "bedroom")
    private Integer bedroom;

    @Column(name = "balcony")
    private Integer balcony;

    @Column(name = "landscape_view", length = 255)
    private String landscapeView;

    @Column(name = "apart_loca")
    private Integer apartLoca;

    @Column(name = "apart_type")
    private Integer apartType;

    @Column(name = "furniture_type")
    private Integer furnitureType;

    @Column(name = "price_rent")
    private Integer priceRent;

    @Column(name = "return_rate")
    private Double returnRate;

    @Column(name = "legal_doc")
    private Integer legalDoc;

    @Column(name = "description", length = 2000)
    private String description;

    @Column(name = "width_y")
    private Integer widthY;

    @Column(name = "long_x")
    private Integer longX;

    @Column(name = "street_house")
    private Integer streetHouse;

    @Column(name = "FSBO")
    private Integer fsbo;

    @Column(name = "view_num")
    private Integer viewNum;

    @Column(name = "create_by")
    private Integer createBy;

    @Column(name = "update_by")
    private Integer updateBy;

    @Column(name = "shape", length = 200)
    private String shape;

    @Column(name = "distance2facade")
    private Integer distanceToFacade;

    @Column(name = "adjacent_facade_num")
    private Integer adjacentFacadeNum;

    @Column(name = "adjacent_road", length = 200)
    private String adjacentRoad;

    @Column(name = "alley_min_width")
    private Integer alleyMinWidth;

    @Column(name = "adjacent_alley_min_width")
    private Integer adjacentAlleyMinWidth;

    @Column(name = "factor")
    private Integer factor;

    @Column(name = "structure", length = 2000)
    private String structure;

    @Column(name = "DTSXD")
    private Integer dtsxd;

    @Column(name = "CLCL")
    private Integer clcl;

    @Column(name = "CTXD_price")
    private Integer ctxdPrice;

    @Column(name = "CTXD_value")
    private Integer ctxdValue;

    @Column(name = "photo", length = 2000)
    private String photo;

    @Column(name = "_lat")
    private Double latitude;

    @Column(name = "_lng")
    private Double longitude;
    @Column(name = "process")
    private String proccess = "pending";

    public RealEstate() {
    }

    public RealEstate(String title, Integer type, Integer request, Integer provinceId, Integer districtId,
            Integer wardsId, Integer streetId, Integer projectId, String address, Integer customerId, BigDecimal price,
            BigDecimal priceMin, Integer priceTime, BigDecimal acreage, Integer direction,
            Integer totalFloors, Integer numberFloors, Integer bath, String apartCode, BigDecimal wallArea,
            Integer bedroom, Integer balcony, String landscapeView, Integer apartLoca, Integer apartType,
            Integer furnitureType, Integer priceRent, Double returnRate, Integer legalDoc, String description,
            Integer widthY, Integer longX, Integer streetHouse, Integer fsbo, Integer viewNum, Integer createBy,
            Integer updateBy, String shape, Integer distanceToFacade, Integer adjacentFacadeNum, String adjacentRoad,
            Integer alleyMinWidth, Integer adjacentAlleyMinWidth, Integer factor, String structure, Integer dtsxd,
            Integer clcl, Integer ctxdPrice, Integer ctxdValue, String photo, Double latitude, Double longitude,
            String proccess) {
        this.title = title;
        this.type = type;
        this.request = request;
        this.provinceId = provinceId;
        this.districtId = districtId;
        this.wardsId = wardsId;
        this.streetId = streetId;
        this.projectId = projectId;
        this.address = address;
        this.customerId = customerId;
        this.price = price;
        this.priceMin = priceMin;
        this.priceTime = priceTime;
        this.acreage = acreage;
        this.direction = direction;
        this.totalFloors = totalFloors;
        this.numberFloors = numberFloors;
        this.bath = bath;
        this.apartCode = apartCode;
        this.wallArea = wallArea;
        this.bedroom = bedroom;
        this.balcony = balcony;
        this.landscapeView = landscapeView;
        this.apartLoca = apartLoca;
        this.apartType = apartType;
        this.furnitureType = furnitureType;
        this.priceRent = priceRent;
        this.returnRate = returnRate;
        this.legalDoc = legalDoc;
        this.description = description;
        this.widthY = widthY;
        this.longX = longX;
        this.streetHouse = streetHouse;
        this.fsbo = fsbo;
        this.viewNum = viewNum;
        this.createBy = createBy;
        this.updateBy = updateBy;
        this.shape = shape;
        this.distanceToFacade = distanceToFacade;
        this.adjacentFacadeNum = adjacentFacadeNum;
        this.adjacentRoad = adjacentRoad;
        this.alleyMinWidth = alleyMinWidth;
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
        this.factor = factor;
        this.structure = structure;
        this.dtsxd = dtsxd;
        this.clcl = clcl;
        this.ctxdPrice = ctxdPrice;
        this.ctxdValue = ctxdValue;
        this.photo = photo;
        this.latitude = latitude;
        this.longitude = longitude;
        this.proccess = proccess;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setDateCreate(Date dateCreate) {
        this.dateCreate = dateCreate;
    }

    public String getProccess() {
        return proccess;
    }

    public void setProccess(String proccess) {
        this.proccess = proccess;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }

    public Integer getRequest() {
        return request;
    }

    public void setRequest(Integer request) {
        this.request = request;
    }

    public Integer getProvinceId() {
        return provinceId;
    }

    public void setProvinceId(Integer provinceId) {
        this.provinceId = provinceId;
    }

    public Integer getDistrictId() {
        return districtId;
    }

    public void setDistrictId(Integer districtId) {
        this.districtId = districtId;
    }

    public Integer getWardsId() {
        return wardsId;
    }

    public void setWardsId(Integer wardsId) {
        this.wardsId = wardsId;
    }

    public Integer getStreetId() {
        return streetId;
    }

    public void setStreetId(Integer streetId) {
        this.streetId = streetId;
    }

    public Integer getProjectId() {
        return projectId;
    }

    public void setProjectId(Integer projectId) {
        this.projectId = projectId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Integer getCustomerId() {
        return customerId;
    }

    public void setCustomerId(Integer customerId) {
        this.customerId = customerId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPriceMin() {
        return priceMin;
    }

    public void setPriceMin(BigDecimal priceMin) {
        this.priceMin = priceMin;
    }

    public Integer getPriceTime() {
        return priceTime;
    }

    public void setPriceTime(Integer priceTime) {
        this.priceTime = priceTime;
    }

    public Date getDateCreate() {
        return dateCreate;
    }

    public BigDecimal getAcreage() {
        return acreage;
    }

    public void setAcreage(BigDecimal acreage) {
        this.acreage = acreage;
    }

    public Integer getDirection() {
        return direction;
    }

    public void setDirection(Integer direction) {
        this.direction = direction;
    }

    public Integer getTotalFloors() {
        return totalFloors;
    }

    public void setTotalFloors(Integer totalFloors) {
        this.totalFloors = totalFloors;
    }

    public Integer getNumberFloors() {
        return numberFloors;
    }

    public void setNumberFloors(Integer numberFloors) {
        this.numberFloors = numberFloors;
    }

    public Integer getBath() {
        return bath;
    }

    public void setBath(Integer bath) {
        this.bath = bath;
    }

    public String getApartCode() {
        return apartCode;
    }

    public void setApartCode(String apartCode) {
        this.apartCode = apartCode;
    }

    public BigDecimal getWallArea() {
        return wallArea;
    }

    public void setWallArea(BigDecimal wallArea) {
        this.wallArea = wallArea;
    }

    public Integer getBedroom() {
        return bedroom;
    }

    public void setBedroom(Integer bedroom) {
        this.bedroom = bedroom;
    }

    public Integer getBalcony() {
        return balcony;
    }

    public void setBalcony(Integer balcony) {
        this.balcony = balcony;
    }

    public String getLandscapeView() {
        return landscapeView;
    }

    public void setLandscapeView(String landscapeView) {
        this.landscapeView = landscapeView;
    }

    public Integer getApartLoca() {
        return apartLoca;
    }

    public void setApartLoca(Integer apartLoca) {
        this.apartLoca = apartLoca;
    }

    public Integer getApartType() {
        return apartType;
    }

    public void setApartType(Integer apartType) {
        this.apartType = apartType;
    }

    public Integer getFurnitureType() {
        return furnitureType;
    }

    public void setFurnitureType(Integer furnitureType) {
        this.furnitureType = furnitureType;
    }

    public Integer getPriceRent() {
        return priceRent;
    }

    public void setPriceRent(Integer priceRent) {
        this.priceRent = priceRent;
    }

    public Double getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(Double returnRate) {
        this.returnRate = returnRate;
    }

    public Integer getLegalDoc() {
        return legalDoc;
    }

    public void setLegalDoc(Integer legalDoc) {
        this.legalDoc = legalDoc;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getWidthY() {
        return widthY;
    }

    public void setWidthY(Integer widthY) {
        this.widthY = widthY;
    }

    public Integer getLongX() {
        return longX;
    }

    public void setLongX(Integer longX) {
        this.longX = longX;
    }

    public Integer getStreetHouse() {
        return streetHouse;
    }

    public void setStreetHouse(Integer streetHouse) {
        this.streetHouse = streetHouse;
    }

    public Integer getFsbo() {
        return fsbo;
    }

    public void setFsbo(Integer fsbo) {
        this.fsbo = fsbo;
    }

    public Integer getViewNum() {
        return viewNum;
    }

    public void setViewNum(Integer viewNum) {
        this.viewNum = viewNum;
    }

    public Integer getCreateBy() {
        return createBy;
    }

    public void setCreateBy(Integer createBy) {
        this.createBy = createBy;
    }

    public Integer getUpdateBy() {
        return updateBy;
    }

    public void setUpdateBy(Integer updateBy) {
        this.updateBy = updateBy;
    }

    public String getShape() {
        return shape;
    }

    public void setShape(String shape) {
        this.shape = shape;
    }

    public Integer getDistanceToFacade() {
        return distanceToFacade;
    }

    public void setDistanceToFacade(Integer distanceToFacade) {
        this.distanceToFacade = distanceToFacade;
    }

    public Integer getAdjacentFacadeNum() {
        return adjacentFacadeNum;
    }

    public void setAdjacentFacadeNum(Integer adjacentFacadeNum) {
        this.adjacentFacadeNum = adjacentFacadeNum;
    }

    public String getAdjacentRoad() {
        return adjacentRoad;
    }

    public void setAdjacentRoad(String adjacentRoad) {
        this.adjacentRoad = adjacentRoad;
    }

    public Integer getAlleyMinWidth() {
        return alleyMinWidth;
    }

    public void setAlleyMinWidth(Integer alleyMinWidth) {
        this.alleyMinWidth = alleyMinWidth;
    }

    public Integer getAdjacentAlleyMinWidth() {
        return adjacentAlleyMinWidth;
    }

    public void setAdjacentAlleyMinWidth(Integer adjacentAlleyMinWidth) {
        this.adjacentAlleyMinWidth = adjacentAlleyMinWidth;
    }

    public Integer getFactor() {
        return factor;
    }

    public void setFactor(Integer factor) {
        this.factor = factor;
    }

    public String getStructure() {
        return structure;
    }

    public void setStructure(String structure) {
        this.structure = structure;
    }

    public Integer getDtsxd() {
        return dtsxd;
    }

    public void setDtsxd(Integer dtsxd) {
        this.dtsxd = dtsxd;
    }

    public Integer getClcl() {
        return clcl;
    }

    public void setClcl(Integer clcl) {
        this.clcl = clcl;
    }

    public Integer getCtxdPrice() {
        return ctxdPrice;
    }

    public void setCtxdPrice(Integer ctxdPrice) {
        this.ctxdPrice = ctxdPrice;
    }

    public Integer getCtxdValue() {
        return ctxdValue;
    }

    public void setCtxdValue(Integer ctxdValue) {
        this.ctxdValue = ctxdValue;
    }

    public String getPhoto() {
        return photo;
    }

    public void setPhoto(String photo) {
        this.photo = photo;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

}
