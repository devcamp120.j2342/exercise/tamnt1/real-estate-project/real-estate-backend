package com.devcamp.user.models;

import javax.persistence.*;

import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;

import java.util.Date;

@Entity
@Table(name = "customers")
@EntityListeners(AuditingEntityListener.class)
public class Customer {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "contact_name", length = 255)
	private String contactName;

	@Column(name = "contact_title", length = 30)
	private String contactTitle;

	@Column(length = 200)
	private String address;

	@Column(length = 80)
	private String mobile;

	@Column(length = 24)
	private String email;

	@Column(length = 5000, name = "note", columnDefinition = "TEXT")
	private String note;
	@Column(name = "create_by")
	private Integer createdBy;
	@Column(name = "update_by")
	private Integer updatedBy;

	@Temporal(TemporalType.TIMESTAMP)
	@CreatedDate
	@Column(name = "create_date")
	private Date createDate;
	@Temporal(TemporalType.TIMESTAMP)
	@LastModifiedDate
	@Column(name = "update_date")
	private Date updateDate;

	public Customer() {
	}

	public Customer(String contactName, String contactTitle, String address, String mobile, String email, String note,
			Integer createdBy, Integer updatedBy) {
		this.contactName = contactName;
		this.contactTitle = contactTitle;
		this.address = address;
		this.mobile = mobile;
		this.email = email;
		this.note = note;
		this.createdBy = createdBy;
		this.updatedBy = updatedBy;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getContactName() {
		return contactName;
	}

	public void setContactName(String contactName) {
		this.contactName = contactName;
	}

	public String getContactTitle() {
		return contactTitle;
	}

	public void setContactTitle(String contactTitle) {
		this.contactTitle = contactTitle;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getMobile() {
		return mobile;
	}

	public void setMobile(String mobile) {
		this.mobile = mobile;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Integer getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(Integer createdBy) {
		this.createdBy = createdBy;
	}

	public Integer getUpdatedBy() {
		return updatedBy;
	}

	public void setUpdatedBy(Integer updatedBy) {
		this.updatedBy = updatedBy;
	}

	public Date getCreateDate() {
		return createDate;
	}

	public void setCreateDate(Date createDate) {
		this.createDate = createDate;
	}

	public Date getUpdateDate() {
		return updateDate;
	}

	public void setUpdateDate(Date updateDate) {
		this.updateDate = updateDate;
	}

}
