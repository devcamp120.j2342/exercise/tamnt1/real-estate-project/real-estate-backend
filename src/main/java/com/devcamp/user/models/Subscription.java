package com.devcamp.user.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "subscriptions")
public class Subscription {

    @Id
    	@GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    @Column(name = "user", length = 255)
    private String user;

    @Column(name = "endpoint", nullable = false, columnDefinition = "LONGTEXT")
    private String endpoint;

    @Column(name = "publickey", length = 255, nullable = false)
    private String publicKey;

    @Column(name = "authenticationtoken", length = 255, nullable = false)
    private String authenticationToken;

    @Column(name = "contentencoding", length = 255, nullable = false)
    private String contentEncoding;

    public Subscription() {
    }

    public Subscription(String user, String endpoint, String publicKey, String authenticationToken,
            String contentEncoding) {
        this.user = user;
        this.endpoint = endpoint;
        this.publicKey = publicKey;
        this.authenticationToken = authenticationToken;
        this.contentEncoding = contentEncoding;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getEndpoint() {
        return endpoint;
    }

    public void setEndpoint(String endpoint) {
        this.endpoint = endpoint;
    }

    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    public String getAuthenticationToken() {
        return authenticationToken;
    }

    public void setAuthenticationToken(String authenticationToken) {
        this.authenticationToken = authenticationToken;
    }

    public String getContentEncoding() {
        return contentEncoding;
    }

    public void setContentEncoding(String contentEncoding) {
        this.contentEncoding = contentEncoding;
    }

}
