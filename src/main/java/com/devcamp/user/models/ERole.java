package com.devcamp.user.models;

public enum ERole {
  ROLE_DEFAULT,
  ROLE_HOMESELLER,
  ROLE_ADMIN
}
