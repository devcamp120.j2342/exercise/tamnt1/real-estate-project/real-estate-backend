package com.devcamp.user.controllers;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.RealEstate;
import com.devcamp.user.response.RealEstateCountResponse;
import com.devcamp.user.response.RealEstateResponse;
import com.devcamp.user.services.RealEstateService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api")
public class RealEstateController {
    private final RealEstateService realEstateService;

    public RealEstateController(RealEstateService realEstateService) {
        this.realEstateService = realEstateService;
    }

    @GetMapping("/real-estates")
    public ResponseEntity<RealEstateResponse> getAllRealEstates(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "120") int size) {
        try {
            RealEstateResponse realEstates = realEstateService.getAllRealEstates(page, size);
            return ResponseEntity.ok(realEstates);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/real-estates/dataTable")
    public ResponseEntity<Object> getAllRealestatesDataTable(
            HttpServletRequest request) {
        try {
            return new ResponseEntity<>(realEstateService.getAllRealestatesDataTable(request),
                    HttpStatus.OK);
        } catch (Exception e) {
            e.printStackTrace();
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/real-estates/active")
    public ResponseEntity<RealEstateResponse> getAllRealEstatesActive(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        try {
            RealEstateResponse realEstates = realEstateService.getAllRealEstatesActive(page, size);
            return ResponseEntity.ok(realEstates);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/real-estates/count")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<List<RealEstateCountResponse>> countRealEstateByPlace() {
        try {
            List<RealEstateCountResponse> realEstates = realEstateService.getRealEstateCount();
            return ResponseEntity.ok(realEstates);
        } catch (ServiceException e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/real-estates/{customerId}/customers")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<RealEstateResponse> getRealEstatesByCustomerId(
            @PathVariable int customerId,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "120") int size) {
        try {
            RealEstateResponse realEstates = realEstateService.getRealEstateByCustomerId(customerId, page, size);
            return ResponseEntity.ok(realEstates);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("real-estates/search")
    public ResponseEntity<RealEstateResponse> searchProperties(
            @RequestParam(required = false) String title,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "120") int size) {
        try {

            RealEstateResponse response = realEstateService.searchProperties(title, page, size);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("real-estates/filter")
    public ResponseEntity<RealEstateResponse> filterProperties(
            @RequestParam(required = false) Integer bedroom,
            @RequestParam(required = false) Integer bath,
            @RequestParam(required = false) Integer provinceId,
            @RequestParam(required = false) Integer price,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        try {
            Pageable pageable = PageRequest.of(page, size);
            RealEstateResponse response = realEstateService.filterProperties(bedroom, bath, provinceId, price,
                    pageable);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("real-estates/customs")
    public ResponseEntity<RealEstateResponse> findRealEstateByProvinceAndDistrict(

            @RequestParam(required = false) Integer districtId,
            @RequestParam(required = false) Integer provinceId,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "10") int size) {
        try {

            RealEstateResponse response = realEstateService.getAllRealEstatesByCustoms(districtId, provinceId,
                    page, size);
            return ResponseEntity.ok(response);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/real-estates/{id}")
    public ResponseEntity<RealEstate> getRealEstateById(@PathVariable int id) {
        try {
            RealEstate realEstate = realEstateService.getRealEstateById(id);
            if (realEstate != null) {
                return ResponseEntity.ok(realEstate);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/real-estates")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<RealEstate> createRealEstate(@RequestBody RealEstate realEstate) {
        try {
            RealEstate createdRealEstate = realEstateService.createRealEstate(realEstate);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdRealEstate);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/real-estates/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<RealEstate> updateRealEstate(@PathVariable int id, @RequestBody RealEstate realEstate) {
        try {
            RealEstate updatedRealEstate = realEstateService.updateRealEstate(realEstate, id);
            if (updatedRealEstate != null) {
                return ResponseEntity.ok(updatedRealEstate);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/real-estates/{id}/process")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<RealEstate> setProcessRealEstate(@PathVariable int id, @RequestBody String processState) {
        try {
            RealEstate updatedRealEstate = realEstateService.setProcess(processState, id);
            if (updatedRealEstate != null) {
                return ResponseEntity.ok(updatedRealEstate);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/real-estates/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<Void> deleteRealEstate(@PathVariable int id) {
        try {
            realEstateService.deleteRealEstate(id);
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
