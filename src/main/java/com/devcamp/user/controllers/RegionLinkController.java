package com.devcamp.user.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.RegionLink;
import com.devcamp.user.services.RegionLinkService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class RegionLinkController {
    private final RegionLinkService regionLinkService;

    public RegionLinkController(RegionLinkService regionLinkService) {
        this.regionLinkService = regionLinkService;
    }

    @GetMapping("/region-links")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<List<RegionLink>> getAllRegionLinks(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "30") int size) {
        try {
            List<RegionLink> regionLinks = regionLinkService.getAllRegionLinks(page, size);
            return ResponseEntity.ok(regionLinks);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/region-links/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<RegionLink> getRegionLinkById(@PathVariable int id) {
        try {
            RegionLink regionLink = regionLinkService.getRegionLinkById(id);
            if (regionLink != null) {
                return ResponseEntity.ok(regionLink);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/region-links")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<RegionLink> createRegionLink(@RequestBody RegionLink regionLink) {
        try {
            RegionLink createdRegionLink = regionLinkService.createRegionLink(regionLink);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdRegionLink);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/region-links/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<RegionLink> updateRegionLink(@PathVariable int id, @RequestBody RegionLink regionLink) {
        try {
            RegionLink updatedRegionLink = regionLinkService.updateRegionLink(regionLink, id);
            if (updatedRegionLink != null) {
                return ResponseEntity.ok(updatedRegionLink);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/region-links/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Void> deleteRegionLink(@PathVariable int id) {
        try {
            regionLinkService.deleteRegionLink(id);
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
