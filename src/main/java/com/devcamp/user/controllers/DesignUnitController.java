package com.devcamp.user.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user.models.DesignUnit;
import com.devcamp.user.services.DesignUnitService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class DesignUnitController {
    private final DesignUnitService designUnitService;

    public DesignUnitController(DesignUnitService designUnitService) {
        this.designUnitService = designUnitService;
    }

    @GetMapping("/design-units")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<List<DesignUnit>> getAllDesignUnits(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "30") int size) {
        try {
            List<DesignUnit> designUnits = designUnitService.getDesignUnits(page, size);
            return ResponseEntity.ok(designUnits);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/design-units/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<DesignUnit> getDesignUnitById(@PathVariable int id) {
        try {
            DesignUnit designUnit = designUnitService.getDesignUnitById(id);
            if (designUnit != null) {
                return ResponseEntity.ok(designUnit);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/design-units")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<DesignUnit> createDesignUnit(@RequestBody DesignUnit designUnit) {
        try {
            DesignUnit createdDesignUnit = designUnitService.createDesignUnit(designUnit);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdDesignUnit);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/design-units/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<DesignUnit> updateDesignUnit(@PathVariable int id, @RequestBody DesignUnit designUnit) {
        try {
            DesignUnit updatedDesignUnit = designUnitService.updateDesignUnit(designUnit, id);
            if (updatedDesignUnit != null) {
                return ResponseEntity.ok(updatedDesignUnit);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/design-units/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Void> deleteDesignUnit(@PathVariable int id) {
        try {
            designUnitService.deleteDesignUnit(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
