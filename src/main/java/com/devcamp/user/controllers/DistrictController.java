package com.devcamp.user.controllers;

import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.devcamp.user.models.District;
import com.devcamp.user.response.DistrictInfo;
import com.devcamp.user.services.DistrictService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class DistrictController {
    private final DistrictService districtService;

    public DistrictController(DistrictService districtService) {
        this.districtService = districtService;
    }

    @GetMapping("/districts")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<List<District>> getAllDistricts(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "20") int size) {
        try {
            List<District> districts = districtService.getAllDistricts(page, size);
            return ResponseEntity.ok(districts);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/districts/province")
    public ResponseEntity<List<DistrictInfo>> getAllDistrictsProvince(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "20") int size) {
        try {
            List<DistrictInfo> districts = districtService.findProvinceAndDistrict(page, size);
            return ResponseEntity.ok(districts);
        } catch (Exception e) {
            e.printStackTrace();
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/districts/{provinceId}/province")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<List<District>> getDistrictsByProvinceId(@PathVariable int provinceId,
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "20") int size) {
        try {
            List<District> updatedDistrict = districtService.getDistrictByProvinceId(provinceId, page, size);
            if (updatedDistrict != null) {
                return ResponseEntity.ok(updatedDistrict);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/districts/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<District> getDistrictById(@PathVariable int id) {
        try {
            District district = districtService.getDistrictById(id);
            if (district != null) {
                return ResponseEntity.ok(district);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/districts")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<District> createDistrict(@RequestBody District district) {
        try {
            District createdDistrict = districtService.createDistrict(district);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdDistrict);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/districts/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<District> updateDistrict(@PathVariable int id, @RequestBody District district) {
        try {
            District updatedDistrict = districtService.updateDistrict(district, id);
            if (updatedDistrict != null) {
                return ResponseEntity.ok(updatedDistrict);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/districts/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Void> deleteDistrict(@PathVariable int id) {
        try {
            districtService.deleteDistrict(id);
            return ResponseEntity.noContent().build();
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
