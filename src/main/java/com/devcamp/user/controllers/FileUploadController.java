package com.devcamp.user.controllers;

import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.devcamp.user.services.FileUpload;

import java.io.IOException;

@Controller
@CrossOrigin
@RequestMapping("/api")
public class FileUploadController {

    private final FileUpload fileUpload;

    public FileUploadController(FileUpload fileUpload) {
        this.fileUpload = fileUpload;
    }

    @GetMapping("/")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public String home() {
        return "home";
    }

    @PostMapping("/upload")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<String> uploadFile(@RequestParam("image") MultipartFile multipartFile) throws IOException {
        String imageURL = fileUpload.uploadFile(multipartFile);
        return ResponseEntity.ok(imageURL);
    }
}