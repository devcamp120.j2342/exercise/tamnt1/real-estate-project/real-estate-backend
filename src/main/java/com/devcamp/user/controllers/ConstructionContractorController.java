package com.devcamp.user.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.ConstructionContractor;
import com.devcamp.user.services.ConstructionContractorService;

@RestController
@CrossOrigin(origins = "*", maxAge = 3600)
@RequestMapping("/api")
public class ConstructionContractorController {
    private final ConstructionContractorService constructionContractorService;

    public ConstructionContractorController(ConstructionContractorService constructionContractorService) {
        this.constructionContractorService = constructionContractorService;
    }

    @GetMapping("/construction-contractors")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")

    public ResponseEntity<List<ConstructionContractor>> getAllConstructionContractors(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "30") int size) {
        try {
            List<ConstructionContractor> constructionContractors = constructionContractorService
                    .getConstructionContractors(page, size);
            return ResponseEntity.ok(constructionContractors);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/construction-contractors/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<ConstructionContractor> getConstructionContractorById(@PathVariable int id) {
        try {
            ConstructionContractor constructionContractor = constructionContractorService
                    .getConstructionContractorById(id);
            if (constructionContractor != null) {
                return ResponseEntity.ok(constructionContractor);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/construction-contractors")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<ConstructionContractor> createConstructionContractor(
            @RequestBody ConstructionContractor constructionContractor) {
        try {
            ConstructionContractor createdConstructionContractor = constructionContractorService
                    .createConstructionContractor(constructionContractor);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdConstructionContractor);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/construction-contractors/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<ConstructionContractor> updateConstructionContractor(
            @PathVariable int id, @RequestBody ConstructionContractor constructionContractor) {
        try {
            ConstructionContractor updatedConstructionContractor = constructionContractorService
                    .updateConstructionContractor(constructionContractor, id);
            if (updatedConstructionContractor != null) {
                return ResponseEntity.ok(updatedConstructionContractor);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/construction-contractors/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<Void> deleteConstructionContractor(@PathVariable int id) {
        try {
            constructionContractorService.deleteConstructionContractor(id);
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
