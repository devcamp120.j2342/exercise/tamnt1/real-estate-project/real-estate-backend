package com.devcamp.user.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.MasterLayout;
import com.devcamp.user.services.MasterLayoutService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class MasterLayoutController {
    private final MasterLayoutService masterLayoutService;

    public MasterLayoutController(MasterLayoutService masterLayoutService) {
        this.masterLayoutService = masterLayoutService;
    }

    @GetMapping("/master-layouts")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<List<MasterLayout>> getAllMasterLayouts(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "30") int size) {
        try {
            List<MasterLayout> masterLayouts = masterLayoutService.getAllMasterLayouts(page, size);
            return ResponseEntity.ok(masterLayouts);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/master-layouts/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<MasterLayout> getMasterLayoutById(@PathVariable int id) {
        try {
            MasterLayout masterLayout = masterLayoutService.getMasterLayoutById(id);
            if (masterLayout != null) {
                return ResponseEntity.ok(masterLayout);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/master-layouts")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<MasterLayout> createMasterLayout(@RequestBody MasterLayout masterLayout) {
        try {
            MasterLayout createdMasterLayout = masterLayoutService.createMasterLayout(masterLayout);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdMasterLayout);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/master-layouts/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<MasterLayout> updateMasterLayout(@PathVariable int id,
            @RequestBody MasterLayout masterLayout) {
        try {
            MasterLayout updatedMasterLayout = masterLayoutService.updateMasterLayout(masterLayout, id);
            if (updatedMasterLayout != null) {
                return ResponseEntity.ok(updatedMasterLayout);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/master-layouts/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Void> deleteMasterLayout(@PathVariable int id) {
        try {
            masterLayoutService.deleteMasterLayout(id);
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
