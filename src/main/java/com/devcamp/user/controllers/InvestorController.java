package com.devcamp.user.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Investor;
import com.devcamp.user.services.InvestorService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class InvestorController {
    private final InvestorService investorService;

    public InvestorController(InvestorService investorService) {
        this.investorService = investorService;
    }

    @GetMapping("/investors")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<List<Investor>> getAllInvestors(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "30") int size) {
        try {
            List<Investor> investors = investorService.getAllInvestors(page, size);
            return ResponseEntity.ok(investors);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/investors/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<Investor> getInvestorById(@PathVariable int id) {
        try {
            Investor investor = investorService.getInvestorById(id);
            if (investor != null) {
                return ResponseEntity.ok(investor);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/investors")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Investor> createInvestor(@RequestBody Investor investor) {
        try {
            Investor createdInvestor = investorService.createInvestor(investor);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdInvestor);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/investors/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Investor> updateInvestor(@PathVariable int id, @RequestBody Investor investor) {
        try {
            Investor updatedInvestor = investorService.updateInvestor(investor, id);
            if (updatedInvestor != null) {
                return ResponseEntity.ok(updatedInvestor);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/investors/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Void> deleteInvestor(@PathVariable int id) {
        try {
            investorService.deleteInvestor(id);
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
