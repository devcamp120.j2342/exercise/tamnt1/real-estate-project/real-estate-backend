package com.devcamp.user.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.AddressMap;
import com.devcamp.user.services.AddressMapService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class AddressMapController {
    private final AddressMapService addressMapService;

    public AddressMapController(AddressMapService addressMapService) {
        this.addressMapService = addressMapService;
    }

    @GetMapping("/address-map")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<List<AddressMap>> getAllAddressMaps(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "30") int size) {
        try {
            List<AddressMap> addressMaps = addressMapService.getAllAddressMaps(page, size);
            return ResponseEntity.ok(addressMaps);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/address-map/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<AddressMap> getAddressMapById(@PathVariable int id) {
        try {
            AddressMap addressMap = addressMapService.getAddressMapById(id);
            if (addressMap != null) {
                return ResponseEntity.ok(addressMap);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/address-map")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<AddressMap> createAddressMap(@RequestBody AddressMap addressMap) {
        try {
            AddressMap createdAddressMap = addressMapService.createAddressMap(addressMap);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdAddressMap);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/address-map/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<AddressMap> updateAddressMaps(@PathVariable int id, @RequestBody AddressMap addressMap) {
        try {
            AddressMap updatedAddressMap = addressMapService.updateAddressMap(id, addressMap);
            return ResponseEntity.ok(updatedAddressMap);

        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/address-map/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Void> deleteAddressMap(@PathVariable int id) {
        try {
            addressMapService.deleteAddressMap(id);
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
