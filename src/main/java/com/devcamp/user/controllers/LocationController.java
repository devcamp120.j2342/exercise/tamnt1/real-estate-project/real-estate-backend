package com.devcamp.user.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Location;
import com.devcamp.user.services.LocationService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class LocationController {
    private final LocationService locationService;

    public LocationController(LocationService locationService) {
        this.locationService = locationService;
    }

    @GetMapping("/locations")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<List<Location>> getAllLocations(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "30") int size) {
        try {
            List<Location> locations = locationService.getAllLocations(page, size);
            return ResponseEntity.ok(locations);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/locations/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN') or hasRole('DEFAULT')")
    public ResponseEntity<Location> getLocationById(@PathVariable int id) {
        try {
            Location location = locationService.getLocationById(id);
            if (location != null) {
                return ResponseEntity.ok(location);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/locations")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Location> createLocation(@RequestBody Location location) {
        try {
            Location createdLocation = locationService.createLocation(location);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdLocation);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/locations/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Location> updateLocation(@PathVariable int id, @RequestBody Location location) {
        try {
            Location updatedLocation = locationService.updateLocation(location, id);
            if (updatedLocation != null) {
                return ResponseEntity.ok(updatedLocation);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/locations/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Void> deleteLocation(@PathVariable int id) {
        try {
            locationService.deleteLocation(id);
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
