package com.devcamp.user.controllers;

import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.user.errors.ServiceException;
import com.devcamp.user.models.Subscription;
import com.devcamp.user.services.SubscriptionService;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class SubscriptionController {
    private final SubscriptionService subscriptionService;

    public SubscriptionController(SubscriptionService subscriptionService) {
        this.subscriptionService = subscriptionService;
    }

    @GetMapping("/subscriptions")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<List<Subscription>> getAllSubscriptions(
            @RequestParam(defaultValue = "0") int page,
            @RequestParam(defaultValue = "30") int size) {
        try {
            List<Subscription> subscriptions = subscriptionService.getAllSubscriptions(page, size);
            return ResponseEntity.ok(subscriptions);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GetMapping("/subscriptions/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Subscription> getSubscriptionById(@PathVariable int id) {
        try {
            Subscription subscription = subscriptionService.getSubscriptionById(id);
            if (subscription != null) {
                return ResponseEntity.ok(subscription);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PostMapping("/subscriptions")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Subscription> createSubscription(@RequestBody Subscription subscription) {
        try {
            Subscription createdSubscription = subscriptionService.createSubscription(subscription);
            return ResponseEntity.status(HttpStatus.CREATED).body(createdSubscription);
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping("/subscriptions/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Subscription> updateSubscription(@PathVariable int id,
            @RequestBody Subscription subscription) {
        try {
            Subscription updatedSubscription = subscriptionService.updateSubscription(subscription, id);
            if (updatedSubscription != null) {
                return ResponseEntity.ok(updatedSubscription);
            } else {
                return ResponseEntity.notFound().build();
            }
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @DeleteMapping("/subscriptions/{id}")
    @PreAuthorize("hasRole('HOMESELLER') or hasRole('ADMIN')")
    public ResponseEntity<Void> deleteSubscription(@PathVariable int id) {
        try {
            subscriptionService.deleteSubscription(id);
            return ResponseEntity.noContent().build();
        } catch (ServiceException e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
